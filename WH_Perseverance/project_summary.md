## William Hopper: A Computational Approach to Perseverance
**Computational/Cognitive Neuroscience**

Three Objectives:
1. Develop a **dual computational/experimental framework** that models the mechanics behind Perseverance
    * What aspects of reward/effort/goal/attention/feedback drive perseverant behaviour?
2. Determine the **neural correlates of Perseverance**
    * Adapt the design for a fMRI experiment
3. Perseverance in **Psychiatric Patients**
    * Apply my work to the problem of treatment non-adherence (specifically in depression/schizophrenia)
    * Only 1 in 5 people will continue anti-depressant medication for more than 4 months - why is this?
