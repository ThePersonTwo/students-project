## Research Project 

> Gruson-Daniel Célya (2018), The french regime of knowledge and its dynamics : open in science and digital technologies in debate. The case study of the french bill for a “digital republic” (2015). Université Paris Descartes https://doi.org/10.5281/zenodo.1491292

under licence CC-BY-SA to download on Zenodo or TEL (in french)



### Abstract : 

This research investigates the worlds of contemporary French knowledge production in order to understand the different meanings of the term ‘open’ in sciences. Specific attention has been drawn to the qualifying adjective ‘open’ in relation to the French translations (ouvert, libre gratuit) as well as associated terms (science, data, access) with this formula. This inquiry began in 2013 and focused mainly on a specific event, the consultation on the bill for a “Digital Republic” (September-October 2015), in particular Article 9 on "open access to scientific publications in public research". This online consultation has allowed for a national and public scope to the issue of access to knowledges. As an “equipped” reality test via a participative website, arose the opportunity to observe almost "live" the defense of different conceptions of "what should be" the contemporary regime of knowledges in France.

Through a grounded theory approach around this particular crystallisation moment of the debates on open in sciences has led me to gradually constitute a corpus of documents, reflecting the deployment of the exchanges on different digital spaces/apparatus (consultation website, scientific blogs, academic notebooks, mainstream press, etc.). Within an iterative research process, I combined digital methods (digital mapping of the similarity of votes) and qualitative analysis of the corpus, as well as the theoretical concepts mobilized at the crossroads between information and communication sciences and “pragmatic sociology of critique”.

This enabled the development of a model which shows that the argumentative perspectives and the strategies in the test implemented by various stakeholders to promote their own conceptions are underpinned by logics, which I have attached to “spirits” of the French regime of knowledges. Subsequently, by switching from modeling to transposable theorization into other fields of research, I show how the distinction between two logics (technoindustrial or processual), behind the discourses on open, can be relevant to analyze the current reconfigurations of other “societal arrangements”. The consultation by itself illustrates this point with the coexistence of two "digital" conceptions of democracy (extended representative or contributive), embodied in the design of the consultative platform.

In the last part, I propose to explain the dynamics between the reconfiguration of a spirit and its social arrangement, by considering the permanent coupling between cognition, technologically mediated actions and socio-technical environment. Finally, the PhD experience narrated throughout this inquiry is also an example of an enaction process on my own conceptions of open. In this sense, it opens further reflections on the situated and incarnated nature of any production of knowledges, which escapes neither the limits nor the potentialities of metacognition.

Keywords: open, regime of knowledges, democracy, debate, digital technologies, enaction

## Open source and open data 

[Github presentation](https://github.com/c24b/openscience4S)

## OPen Access 

Everything is accessible online in different repository HAL, Zenodo mostly [see here](https://celyagd.github.io/publications/)
