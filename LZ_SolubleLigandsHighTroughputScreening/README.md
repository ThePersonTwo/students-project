## PhD Project: Measuring receptors affinity with soluble ligands with high-throughput, microfluidic techniques (*Lorenzo*)

*Collaboration between **ESPCI Paris - PSL** and **Institut Pasteur***.
*Funded by **Ecole Doctoral 388** - Chimie Physique & Chimie Analitique*.

#### Roadmap

 - Develop analytical (ELISA-like) assays in picoliter droplets.
 - Isolate and encapsulate memory lymphocytes in droplets.
 - Measure the affinity of surface receptors to selected ligands (antigen)
 - Screen the immune repertoire of cell lines/individuals towards a target antigen.
 - Validate resolution and limit of detection of techniques
 - Compare benefits and drawbacks with other established techniques (FACS)
 - Investigate features of the immune system, specifically the maturation of high-affinity antibodies after exposure to the antigen.

#### Involved techniques

 - Microfabrication of fluidic chips
 - Controlled production of water-in-oil emulsion, with water droplets of fixed volume
 - Epi-fluorescence wide-field imaging of an huge number of droplets in parallel (>100'000)
 - Recombinant production of white cells expressing standard receptors on the surface
 - Memory B cell sampling from human blood, from mouse blood and other mouse lymphoid organs.

#### Expected outcome/applications

 - An easy to use tool for the assessment of the composition of immune repertoires of individuals at a given time point, from a simple peripheral blood collection.
 - Quantitative information on the effect of vaccination and/or infection on the immune system, screening the response in term of affinity/activities of the induced antibodies.
